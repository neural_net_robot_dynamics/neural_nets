import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import metrics
from sklearn.metrics import explained_variance_score
from pandas import DataFrame
from pandas import concat

# Função para preparação do dataframe:
def datasetPrep (filename, numberOfJoints, firstRowsToDel, lastRowsToDel):
    # Importação do arquivo .csv e atribuição de nomes às colunas: 
    columnNames = []
    for i in range(numberOfJoints):
        columnNames.extend(["Time_1_"+str(i+1), "Torque_"+str(i+1)])
    for i in range(numberOfJoints):
        columnNames.extend(["Time_2_"+str(i+1), "Position_"+str(i+1)])
    df = pd.read_csv(filename, sep='\t', names = columnNames, index_col=False)
    
    # Exclusão das colunas de tempo repetidas:
    df.drop(df.iloc[:, 2:len(columnNames):2], axis=1, inplace=True)
        
    # Derivação da posição para obter velocidade e aceleração:
    clock = 0.012
    for i in range(numberOfJoints):
        df["Velocity_"+str(i+1)] = df["Position_"+str(i+1)].diff() / clock     
    for i in range(numberOfJoints):
        df["Acceleration_"+str(i+1)] = df["Velocity_"+str(i+1)].diff() / clock
        
    # Remoção das primeiras e últimas linhas, nas quais aceleração e velocidade não estão adequadas:
    df = df.drop(df.index[:firstRowsToDel])
    df = df.drop(df.index[-lastRowsToDel:])
        
    return(df)
    

# Função para definir matriz X de váriaveis independentes:
def X_alloc(df, numberOfJoints):
    X = df.iloc[:, (numberOfJoints+1):(21)].values    
    return(X)


# Função para definir vetor y de váriaveis dependentes:
def y_alloc(df, numberOfJoints):
    y = df.iloc[:, 1:(numberOfJoints+1)].values
    return(y)
    

# Função para plotagem dos dados de torque previsto e real de cada junta:
def torquePlotting(torque_estimated, torque_real, numberOfJoints):
    # Vetor de tempo:   
    t = range(len(torque_estimated))   

    # Plotagem dos torques reais e os previstos:
    fig = plt.figure()
    for i in range(numberOfJoints):
        ax_i = fig.add_subplot(numberOfJoints+1, 1, i+1)
        #ax_i.plot(t, torque_real[:,i], t, torque_estimated[:, i])
        ax_i.plot(torque_real[:,i], label='real')
        ax_i.plot(torque_estimated[:, i], label='predicted')
        
        
# Função que imprime estatísticas dos dados previstos e reais de cada junta: 
def evaluateData(numberOfJoints, torque_real, torque_estimated, numberOfNeurons, n_epochs, batchSize): 
    for i in range(numberOfJoints):
        max_value = torque_real[:,i].max()
        min_value = torque_real[:,i].min()
        amplitude = max_value - min_value
        scoreMSE = metrics.mean_squared_error(torque_estimated[:,i], torque_real[:,i])
        score = np.sqrt(scoreMSE)
        ratio = 100 * score / amplitude
        rounded_ratio = "{0:.2f}".format(ratio)
        EVS = "{0:.4f}".format(explained_variance_score(torque_real[:,i], torque_estimated[:,i]))
        R2 = "{0:.4f}".format(metrics.r2_score(torque_real[:,i], torque_estimated[:,i]))
        print("Score final para a junta " + str(i+1) + " é " + str("{0:.4f}".format(score)) + ", " + rounded_ratio + " % da amplitude total.")
        print("Explained variance score: " + str(EVS))
        print("R2 value: " + str(R2)+ "\n")
    print("Explained variance score do conjunto todo: " + str("{0:.4f}".format(explained_variance_score(torque_real, torque_estimated))))
    print("Situação da simulação: Número de Neurons: " + str(numberOfNeurons) + "| Número de epochs: " + str(n_epochs) + "| Batch_size: " + str(batchSize))

# Função que converte séries temporais em aprendizado supervisionado:
# Ver em https://machinelearningmastery.com/convert-time-series-supervised-learning-problem-python/
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ..., t-1):
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
    # Forecast sequence (t, t+1, ..., t+n):
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
    # Put it all together:
    agg = concat(cols, axis=1)
    agg.columns = names
    # Drop rows with NaN values:
    if dropnan:
        agg.dropna(inplace=True)
    return agg