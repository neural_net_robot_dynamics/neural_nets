# LSTM com duas camadas:
from matplotlib import pyplot
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from numpy import concatenate
from math import sqrt
from NN_functions import *
from keras import optimizers


# Váriaveis para preparação do dataframe e perceptrons da rede: 
filename = "path_to_file.csv"
numberOfJoints = 6
numberOfNeurons = 50
n_epochs = 250
batchSize = 32

# Definição do número de linhas do início e fim do dataframe para serem deletadas, default = 1:
firstRowsToDel = 1
lastRowsToDel = 1

 
# Chamada das funรงรตes para criar o dataframe e inputs e outputs da rede neural:    
#df= datasetPrep(filename, numberOfJoints, firstRowsToDel, lastRowsToDel)
df = pd.read_csv(filename, sep='\t', index_col=False, header=None)
values = df.values
X = df.iloc[:, (numberOfJoints+1):(25)].values
y = y_alloc(df, numberOfJoints)

# Split into train and test sets:
train_ratio = 1
n_train_steps = int(len(values)*train_ratio)
train = values[:n_train_steps, :]
test = values[n_train_steps:, :]

# Split into inputs and outputs:
#n_obs = n_lines * n_features
train_X, train_y = train[:, 7:25], train[:, 1:7]
test_X, test_y = test[:, 7:25], test[:, 1:7]
total_X = values[:, 7:25]
total_y = values[:, 1:7]

# Design of the network:
numberOfOutputs = 6
model = Sequential()
model.add(LSTM(numberOfNeurons, return_sequences=True, input_shape=(1, 18)))
model.add(LSTM(numberOfNeurons))
model.add(Dense(numberOfOutputs, activation ='linear'))
model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mse'])
# =============================================================================
# adad = optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)
# model.compile(optimizer=adad, loss='mean_squared_error')
# =============================================================================

# Fit Network:
train_X = train_X.reshape((train_X.shape[0], 1, 18))
test_X = test_X.reshape(test_X.shape[0], 1, 18)
total_X = total_X.reshape((total_X.shape[0], 1, 18))
history = model.fit(train_X, train_y, epochs=n_epochs, batch_size=batchSize, validation_data=(test_X, test_y), verbose=2, shuffle=True)
 

# Insert new data:
newfile = "path_to_new_file.csv"
new_df = pd.read_csv(newfile, sep='\t', index_col=False, header=None)
new_values = new_df.values
new_X = new_df.iloc[:, 7:25]
new_X = new_X.values
new_y = y_alloc(new_df, numberOfJoints)
new_X = new_X.reshape(new_X.shape[0], 1, 18)

# Make a prediction:
torque_estimated = model.predict(total_X)
torque_real = total_y

torquePlotting(torque_estimated, torque_real, numberOfJoints)

evaluateData(numberOfJoints, torque_real, torque_estimated, numberOfNeurons, n_epochs, batchSize)