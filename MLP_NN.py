from NN_functions import *

# [INPUTS DO USUÁRIO]
# Váriaveis para preparação do dataframe e perceptrons da rede: 
filename = "path_to_file.csv"
numberOfJoints = 6
numeroDePercept = 200
batchSize = 64
n_epochs = 400

# Definição do número de linhas ao ínício e fim do dataframe para serem deletadas, default = 1:
firstRowsToDel = 5544
lastRowsToDel = 2


# [CHAMADA DAS FUNÇÕES]    
# Chamada das funções para criar o dataframe e inputs e outputs da rede neural:    
df= datasetPrep(filename, numberOfJoints, firstRowsToDel, lastRowsToDel)
df = pd.read_csv(filename, sep='\t', index_col=False, header=None)
X = X_alloc(df, numberOfJoints)
y = y_alloc(df, numberOfJoints)

# [DATASET SPLITTING]
from sklearn.model_selection import train_test_split


# Separação do dataset entre set para treino e set para teste:
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0, random_state=42)


# [MONTAGEM DA REDE NEURAL MLP]
from keras.models import Sequential
from keras.layers import Dense
from keras import optimizers
from keras import callbacks

# Inicialização da Rede Neural:
model = Sequential()

# Adição das camadas input, primeira e segunda hidden layers:
model.add(Dense(activation ='relu', units=numeroDePercept, kernel_initializer='uniform', input_dim=X.shape[1]))
model.add(Dense(activation ='relu', units=numeroDePercept, kernel_initializer='uniform'))

# Adição da camada output:
model.add(Dense(activation ='linear', units=y.shape[1], kernel_initializer='uniform'))


# [COMPILAÇÃO DA REDE NEURAL]
# Selecionar o tipo de otimizador dentro da função:
rms = optimizers.RMSprop(lr=0.0001, rho=0.9, epsilon=None, decay=0.0)
adad = optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)
adam = optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
model.compile(optimizer=adam, loss='mean_squared_error')
monitor = callbacks.EarlyStopping(monitor='val_loss', min_delta=1e-3, patience=5, verbose=1, mode='auto')


# [FIT DA REDE NEURAL NO TRAINING SET]
model.fit(X_train, y_train, batch_size=batchSize, epochs=n_epochs, verbose=2, callbacks=[monitor])


# [PLOTAGEM E AVALIAÇÃO DOS DADOS]
# Predição de torques a partir da rede neural gerada:

# Inserção de novo arquivo para avaliação 
newfile = "path_to_new_file"
new_firstRowsToDel = 1679
new_lastRowsToDel = 5
new_df = datasetPrep(newfile, numberOfJoints, new_firstRowsToDel, new_lastRowsToDel)
new_df = pd.read_csv(newfile, sep='\t', index_col=False, header=None)
new_X = X_alloc(new_df, numberOfJoints)
new_y = y_alloc(new_df, numberOfJoints)

# Criação das variáveis de input para avaliação. Se for inserir o novo arquivo, altere X para new_X e y para new_y
X_toPredict = X
torque_real = y
torque_estimated = model.predict(X_toPredict)
        
torquePlotting(torque_estimated, torque_real, numberOfJoints)

evaluateData(numberOfJoints, torque_real, torque_estimated, numeroDePercept, n_epochs, batchSize)

