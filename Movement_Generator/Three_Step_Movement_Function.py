from robot_kuka import *
from models import Axes
from random import choice
import math
import matplotlib.pyplot as plt

kr16 = RobotKuka()
kr16.start_communication("192.168.10.15", 6008)
kr16.wait_robot()
kr16.sleep(0.5)

amp6 = 30
amp5 = 30
amp4 = 30
amp3 = 30
amp2 = 30
amp1 = 30
deltat = 0.75
omega = 20

# envio de comandos:
for k in range(100):
    kr16.rotate_axes(exec_time = 7.0, axes=axes(a6 = amp6 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a5 = amp5 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)), 
                                                a4 = amp4 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a3 = amp3 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a2 = amp2 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a1 = amp1 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)))

omega = 40
for k in range(100):
    kr16.rotate_axes(exec_time = 7.0, axes=axes(a6 = amp6 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a5 = amp5 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)), 
                                                a4 = amp4 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a3 = amp3 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a2 = amp2 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a1 = amp1 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)))

omega = 60                                            
for k in range(100):
    kr16.rotate_axes(exec_time = 7.0, axes=axes(a6 = amp6 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a5 = amp5 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)), 
                                                a4 = amp4 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a3 = amp3 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a2 = amp2 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)),
                                                a1 = amp1 * (math.sin(k*omega*deltat) + math.sin(k*omega/2*deltat)))
                                                

t = range(len(a6))
plt.plot(t, a6, t, a5)

# kr16.sleep(1200)
# desativa o controle de impedancia
# kr16.deactivate_impedance_control()
kr16.sleep(0.1)
kr16.stop_communication()
